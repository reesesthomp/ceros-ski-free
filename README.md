# Ceros Ski Code Challenge

Welcome to the Ceros Code Challenge - Ski Edition!

For this challenge, we have included some base code for Ceros Ski, our version of the classic Windows game SkiFree. If
you've never heard of SkiFree, Google has plenty of examples. Better yet, you can play our version here: 
http://ceros-ski.herokuapp.com/

# How to Play

Please visit [Ceros Ski On the Web](http://cerosskiapp-env.3pfe8a8mqm.us-east-2.elasticbeanstalk.com/)!

Controls:
 - Down: Speed directly down the mountain
 - Left-Right: Turn skier, and make some wicked S curves
 - Up: To climb back up the mountain, make sure your stopped!
 - Space: Jump and perform some gnarly tricks, or to clear rocks (Trees are a little too tall to jump dude!)

Goal:
 - See how long you can shred pow before smashing into some trees or rocks

# Core Engine

This project utilizes a core game engine.  The engine is responsible for scene and game entity management, and will handle
frame updates, collision, and keydown events accordingly.  Please review the folling documentation.

## Scene

Game scenes dictate the creation and management of game entities, and handle scene specific updates.  These may include camera draws,
entity creation, and global keydown events.  The following methods are called from the game engine:

 - `start` - Method that fires before first frame update
 - `updateScene` - Method is called once per frame, before draw
 - `drawScene` - Method is called once per frame
 - `onSceneKeyDown` - Method is called on keydown events

**NOTE**
- Keys must be added to the Constants.KEYS for binding and correct browser behavior

## Game Entity

Game entities have the ability to implement methods that will be called from the core game engine each frame.  These include:

 - `update` - Called per frame for and frame updates
 - `onCollision(collider)` - When implemented, the entitiy will check for collisions.  If collision is detected, this method will fire
 - `draw(canvas, assetManager)` - *Super* - Draws the current entity asset at entity.x and entity.y
 - `onKeyDown(event)` - When implemented, this will fire on a bound key press

# Developer

## Installation ##

**Prerequisites**

[Node.js](https://nodejs.org/en/) - To ensure version compatability, please see [NVM](https://github.com/nvm-sh/nvm)

**Recommended**

For CI/CD testing and containerization, see [Docker](https://www.docker.com/products/docker-desktop)
This project uses the `python:latest` image in it's CI/CD pipelines: See [Pipelines]

**Install**

- Clone repository
- `npm install` in project directory

**Run**

- Dev - Builds project and starts server on [localhost:8080](http://localhost:8080) - `npm run dev`
- Test - Runs tests using [Jest](https://jestjs.io/) - `npm run test`
- Build - Builds project for production - `npm run build`
- Start - Starts a basic web-server that will serve from a built `dist` directory

## CI/CD ##

[GitLab pipelines](https://docs.gitlab.com/ee/ci/pipelines.html) have been turned on for this project.  Please note the following CI/CD builds:

**All Branches**

Pipeline: TEST

- All branches will be tested via Jest and must pass all tests before merging into develop branch

Pipeline: BUILD & DEPLOY

- Master branch only
- This pipline will test, build, then deploy the project to our elastic beanstalk instance, found [here.](http://cerosskiapp-env.3pfe8a8mqm.us-east-2.elasticbeanstalk.com/)
- AWS service worker credentials are found in SETTINGS -> CI/CD -> VARIABLES
- Please contact the AWS administrator for EB access ReeseSThomp@gmail.com.

# Project Requirements - WIP #

**Bonus**

*Note: You won’t be marked down for excluding any of this, it’s purely bonus.  If you’re really up against the clock, 
make sure you complete all of the listed requirements and to focus on writing clean, well organized, well documented 
code before taking on any of the bonus.*

If you're having fun with this, feel free to add more to it. Here's some ideas or come up with your own. We love seeing 
how creative candidates get with this.
 
* Provide a way to reset the game once it's over
* Provide a way to pause and resume the game
* Add a score that increments as the skier skis further
* Increase the difficulty the longer the skier skis (increase speed, increase obstacle frequency, etc.)
* Deploy the game to a server so that we can play it without having to install it locally
* Write more unit tests for your code

We are looking forward to see what you come up with!
