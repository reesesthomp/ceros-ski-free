import '../css/game.css';
import Engine from './Core/Engine'
import Game from './Scenes/Game';

document.addEventListener("DOMContentLoaded",() => {
    Engine.load().then(() => {
        Engine.startScene(Game);
    });
});