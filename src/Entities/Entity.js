export class Entity {
    x = 0;
    y = 0;

    assetName = '';

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    getAssetName() {
        return this.assetName;
    }

    getPosition() {
        return {
            x: this.x,
            y: this.y
        };
    }

    animate(animationFrames, animationTiming, animationCompleteCallback, doesAnimationLoop = false) {
        this.stopAnimation();
        // Find frame switch speed based on length of animation
        // Verify: Frames should have equal timing
        const numberOfFrames = animationFrames.length;
        const animationFrameCycleTime = animationTiming / numberOfFrames;
        // Begin loop through animation frames
        let intervalIdx = 0;
        this.assetName = animationFrames[intervalIdx];
        this.animationInterval = setInterval (() => {
            intervalIdx++;
            if (intervalIdx < numberOfFrames) {
                this.assetName = animationFrames[intervalIdx];
            } else {
                this.stopAnimation();
                if (doesAnimationLoop) {
                    // Infinitely recursive if animation loops, unless stopAnimation is called;
                    this.animate(animationFrames, animationTiming, animationCompleteCallback, doesAnimationLoop);
                }
                // Callback will fire on each successful animation loop
                if (typeof animationCompleteCallback === 'function') {
                    animationCompleteCallback();
                }
            }
        }, animationFrameCycleTime);
    }

    stopAnimation() {
        clearInterval(this.animationInterval);
    }

    draw(canvas, assetManager) {
        const asset = assetManager.getAsset(this.assetName);
        const drawX = this.x - asset.width / 2;
        const drawY = this.y - asset.height / 2;

        canvas.drawImage(asset, drawX, drawY, asset.width, asset.height);
    }
}