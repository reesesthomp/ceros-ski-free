import * as Constants from "../../Core/Constants";
import { Entity } from "../Entity";
import { getDistance } from "../../Core/Utils";
import Game from "../../Scenes/Game";

export class Rhino extends Entity {
    assetName = Constants.RHINO_DEFAULT;
    speed = Constants.ENEMY_STARTING_SPEED;
    
    // State properties
    isChasing = true;
    isFacingLeft = true;

    constructor(x, y, skier) {
        super(x, y);
        this.skier = skier;
        this.distance = getDistance(this, this.skier);
        this.animate(Constants.ANIMATIONS.RHINO_RUN_L_ANIM, 500, this.updateRunAnimation.bind(this));
    }

    defaultState() {
        this.isChasing = true;
        this.isFacingLeft = true;
    }

    update() {
        if (this.isChasing) {
            this.distance = getDistance(this, this.skier);
            if (this.distance < (this.speed * 2)) {
                this.isChasing = false;
                this.caughtPlayer();
            } else {
                this.move();
            }
        }
    }

    updateRunAnimation() {
        if (this.isFacingLeft) {
            this.animate(Constants.ANIMATIONS.RHINO_RUN_L_ANIM, 500, this.updateRunAnimation.bind(this));
        } else {
            this.animate(Constants.ANIMATIONS.RHINO_RUN_R_ANIM, 500, this.updateRunAnimation.bind(this));
        }
    }

    move() {
        
        if (this.x - this.skier.x < 0 - this.speed) {
            this.isFacingLeft = false;
            if (this.y - this.skier.y < 0 - this.speed) {
                this.moveRightDown();
            } else if (this.y - this.skier.y > 0 + this.speed) {
                this.moveRightUp();
            } else {
                this.moveRight();
            }
        } else if (this.x - this.skier.x > 0 + this.speed) {
            this.isFacingLeft = true;
            if (this.y - this.skier.y < 0 - this.speed) {
                this.moveLeftDown();
            } else if (this.y - this.skier.y > 0 + this.speed) {
                this.moveLeftUp();
            } else {
                this.moveLeft();
            }
        } else {
            if (this.y - this.skier.y < 0 - this.speed) {
                this.moveDown();
            } else if (this.y - this.skier.y > 0 + this.speed) {
                this.moveUp();
            }
        }
    }

    moveLeft() {
        this.x -= this.speed;
    }

    moveLeftDown() {
        this.x -= this.speed / Constants.SPEED_NORMALIZER;
        this.y += this.speed / Constants.SPEED_NORMALIZER;
    }

    moveLeftUp() {
        this.x -= this.speed / Constants.SPEED_NORMALIZER;
        this.y -= this.speed / Constants.SPEED_NORMALIZER;
    }

    moveRight() {
        this.x += this.speed;
    }

    moveRightDown() {
        this.x += this.speed / Constants.SPEED_NORMALIZER;
        this.y += this.speed / Constants.SPEED_NORMALIZER;
    }

    moveRightUp() {
        this.x += this.speed / Constants.SPEED_NORMALIZER;
        this.y -= this.speed / Constants.SPEED_NORMALIZER;
    }

    moveDown() {
        this.y += this.speed;
    }

    moveUp() {
        this.y -= this.speed;
    }

    caughtPlayer() {
        Game.destroyGameEntity(Game.skier);
        this.animate(Constants.ANIMATIONS.RHINO_CATCH_ANIM, 1000, this.startCelebration.bind(this));
    }

    startCelebration() {
        console.log('Cele')
        this.animate(Constants.ANIMATIONS.RHINO_CELEBRATE_ANIM, 500, null, true);
    }
}