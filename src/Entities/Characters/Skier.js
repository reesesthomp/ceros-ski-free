import * as Constants from "../../Core/Constants";
import { Entity } from "../Entity";

export class Skier extends Entity {
    assetName = Constants.SKIER_DOWN;

    direction = Constants.SKIER_DIRECTIONS.DOWN;
    speed = Constants.SKIER_STARTING_SPEED;

    // State properties
    isSkierJump = false;

    constructor(x, y) {
        super(x, y);
    }

    defaultState() {
        this.isSkierJump = false;
    }

    update() {
        this.move();
    }

    onCollision(collider) {
        if (collider.assetName == Constants.TREE || collider.assetName == Constants.TREE_CLUSTER) {
            this.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
            this.defaultState();
        }
        if (collider.assetName == Constants.ROCK1 || collider.assetName == Constants.ROCK2) {
            if (this.isSkierJump) {
                // Skier can jump over rocks
                return;
            } else {
                this.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
                this.defaultState();
            }
        }
        if (collider.assetName == Constants.RAMP) {
            if (!this.isSkierJump) {
                // Skier can jump over jumps
                this.jump();
            }
        }
    }

    onKeyDown(keycode) {
        if (this.isSkierJump) {
            // Verify: Skier cannot change directions midair
            return;
        };
        switch(keycode) {
            case Constants.KEYS.LEFT:
                this.turnLeft();
                break;
            case Constants.KEYS.RIGHT:
                this.turnRight();
                break;
            case Constants.KEYS.UP:
                this.turnUp();
                break;
            case Constants.KEYS.DOWN:
                this.turnDown();
                break;
            case Constants.KEYS.SPACE:
                this.jump();
        }
    }

    setDirection(direction) {
        this.direction = direction;
        this.updateAsset();
    }

    updateAsset() {
        this.assetName = Constants.SKIER_DIRECTION_ASSET[this.direction];
    }

    move() {
        switch(this.direction) {
            case Constants.SKIER_DIRECTIONS.LEFT_DOWN:
                this.moveSkierLeftDown();
                break;
            case Constants.SKIER_DIRECTIONS.DOWN:
                this.moveSkierDown();
                break;
            case Constants.SKIER_DIRECTIONS.RIGHT_DOWN:
                this.moveSkierRightDown();
                break;
        }
    }

    moveSkierLeft() {
        this.x -= this.speed;
    }

    moveSkierLeftDown() {
        this.x -= this.speed / Constants.SPEED_NORMALIZER;
        this.y += this.speed / Constants.SPEED_NORMALIZER;
    }

    moveSkierDown() {
        this.y += this.speed;
    }

    moveSkierRightDown() {
        this.x += this.speed / Constants.SPEED_NORMALIZER;
        this.y += this.speed / Constants.SPEED_NORMALIZER;
    }

    moveSkierRight() {
        this.x += this.speed;
    }

    moveSkierUp() {
        this.y -= this.speed;
    }

    turnLeft() {
        if(this.direction === Constants.SKIER_DIRECTIONS.LEFT) {
            this.moveSkierLeft();
        }
        else if (this.direction === Constants.SKIER_DIRECTIONS.CRASH) {
            this.setDirection(Constants.SKIER_DIRECTIONS.LEFT)
            this.moveSkierLeft();
        }
        else {
            // This moves (neg) through an enum.  May throw undefined ref if < 0;
            this.setDirection(this.direction - 1);
        }
    }

    turnRight() {
        if(this.direction === Constants.SKIER_DIRECTIONS.RIGHT) {
            this.moveSkierRight();
        }
        else if (this.direction === Constants.SKIER_DIRECTIONS.CRASH) {
            this.setDirection(Constants.SKIER_DIRECTIONS.RIGHT)
            this.moveSkierRight();
        }
        else {
            // This moves (pos) through an enum.  May throw undefined ref if > 5;
            this.setDirection(this.direction + 1);
        }
    }

    turnUp() {
        if(this.direction === Constants.SKIER_DIRECTIONS.LEFT || this.direction === Constants.SKIER_DIRECTIONS.RIGHT) {
            this.moveSkierUp();
        }
    }

    turnDown() {
        this.setDirection(Constants.SKIER_DIRECTIONS.DOWN);
    }

    jump() {
        // Verify: Skier will face downhill on jump
        this.speed += Constants.ADDITIONAL_JUMP_SPEED;
        this.direction = Constants.SKIER_DIRECTIONS.DOWN;
        this.isSkierJump = true;
        this.animate(Constants.ANIMATIONS.SKIER_JUMP_ANIM, 750, this.endJump.bind(this));
    }

    endJump() {
        this.speed -= Constants.ADDITIONAL_JUMP_SPEED
        // Verify: Skier will face downhill when landing from jump
        this.setDirection(Constants.SKIER_DIRECTIONS.DOWN);
        this.defaultState();
    }

}