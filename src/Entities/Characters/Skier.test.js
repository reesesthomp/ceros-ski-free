import "babel-polyfill";
import * as Constants from "../../Core/Constants";
import { Skier } from './Skier';

describe("The skiier entity", () => {
    let skierX = 25,
        skierY = 35;

    // Init standalone skier object
    const skier = new Skier(skierX, skierY);

    // Properties
    it ('should initialize with correct x', () => {
        expect(skier.x).toEqual(skierX);
    });
    it ('should initialize with correct y', () => {
        expect(skier.y).toEqual(skierY);
    });
    it ('should instantiate with correct asset name', () => {
        expect(skier.assetName).toEqual(Constants.SKIER_DOWN);
    });
    it ('should instantiate with correct direction', () => {
        expect(skier.direction).toEqual(Constants.SKIER_DIRECTIONS.DOWN);
    });
    it ('should instantiate with correct speed', () => {
        expect(skier.speed).toEqual(Constants.SKIER_STARTING_SPEED);
    });

    // Methods
    it ('should ski downwards on down press', () => {
        skier.turnDown();
        expect(skier.direction).toEqual(Constants.SKIER_DIRECTIONS.DOWN);
        expect(skier.assetName).toEqual(Constants.SKIER_DIRECTION_ASSET[Constants.SKIER_DIRECTIONS.DOWN]);
    });
    it ('should rotate left on left press, but only until it is facing left', () => {
        // Set skier state: Facing right, going right
        skier.assetName = Constants.SKIER_RIGHT;
        skier.direction = Constants.SKIER_DIRECTIONS.RIGHT;

        // Skier facing right-down
        skier.turnLeft();
        expect(skier.assetName).toBe(Constants.SKIER_RIGHTDOWN);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.RIGHT_DOWN);

        // Skier facing down
        skier.turnLeft();
        expect(skier.assetName).toBe(Constants.SKIER_DOWN);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.DOWN);

        // Skier facing left-down
        skier.turnLeft();
        expect(skier.assetName).toBe(Constants.SKIER_LEFTDOWN);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.LEFT_DOWN);

        // Skier facing left
        skier.turnLeft();
        expect(skier.assetName).toBe(Constants.SKIER_LEFT);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.LEFT);

        // Skier does not change
        skier.turnLeft();
        expect(skier.assetName).toBe(Constants.SKIER_LEFT);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.LEFT);
    });
    it ('should rotate right on right press, but only until it is facing right', () => {
        // Set skier state: Facing left, going left
        skier.assetName = Constants.SKIER_LEFT;
        skier.direction = Constants.SKIER_DIRECTIONS.LEFT;

        // Skier facing left-down
        skier.turnRight();
        expect(skier.assetName).toBe(Constants.SKIER_LEFTDOWN);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.LEFT_DOWN);

        // Skier facing down
        skier.turnRight();
        expect(skier.assetName).toBe(Constants.SKIER_DOWN);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.DOWN);

        // Skier facing right-down
        skier.turnRight();
        expect(skier.assetName).toBe(Constants.SKIER_RIGHTDOWN);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.RIGHT_DOWN);

        // Skier facing right
        skier.turnRight();
        expect(skier.assetName).toBe(Constants.SKIER_RIGHT);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.RIGHT);

        // Skier does not change
        skier.turnRight();
        expect(skier.assetName).toBe(Constants.SKIER_RIGHT);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.RIGHT);
    });
    it ('should crash (but not the code)', () => {
        skier.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
        expect(skier.assetName).toBe(Constants.SKIER_DIRECTION_ASSET[Constants.SKIER_DIRECTIONS.CRASH]);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.CRASH);
    });
    it ('should recover from crashes', () => {
        // Set Skier crashed
        skier.setDirection(Constants.SKIER_DIRECTIONS.CRASH);

        // Crash move left
        skier.turnLeft();
        expect(skier.assetName).toBe(Constants.SKIER_DIRECTION_ASSET[Constants.SKIER_DIRECTIONS.LEFT]);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.LEFT);
        
        // Set Skier crashed
        skier.setDirection(Constants.SKIER_DIRECTIONS.CRASH);

        // Crash move right
        skier.turnRight();
        expect(skier.assetName).toBe(Constants.SKIER_DIRECTION_ASSET[Constants.SKIER_DIRECTIONS.RIGHT]);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.RIGHT);
    });
    it ('should jump on space press', () => {
        // TODO: Mimic space press
        // TODO: Canvas tests - RESEARCH: jest-pupeteer
        skier.jump();

        expect(skier.isSkierJump).toBeTruthy();
    });
});