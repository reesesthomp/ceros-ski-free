import * as Constants from "../Core/Constants";
import { Skier } from "../Entities/Characters/Skier";
import { ObstacleManager } from "../Entities/Obstacles/ObstacleManager";
import { Rect } from '../Core/Utils';
import Engine from '../Core/Engine';
import { Rhino } from "../Entities/Characters/Rhino";

class Game {

    gameWindow = {};
    previousGameWindow = {};

    constructor() {
        this.obstacleManager = new ObstacleManager();
        this.skier = new Skier(0,0);
        Engine.entities.push(this.skier);
    }

    start() {
        this.obstacleManager.placeInitialObstacles();
    }

    updateScene() {
        // Generate new obstacle
        this.obstacleManager.placeNewObstacle(this.gameWindow, this.previousGameWindow);

        // Introduce enemy at time
        if (!this.rhino) {
            if (Engine.time >= Constants.ENEMY_ENTRY_TIME) {
                this.rhino = new Rhino(this.skier.x + Constants.GAME_WIDTH, this.skier.y, this.skier);
                Engine.entities.push(this.rhino);
            }
        } else {
            // Slightly increast enemy speed as time passes
            this.rhino.speed += Constants.ENEMY_SPEED_GAIN;
        }
    }

    drawScene() {
        const skierPosition = this.skier.getPosition();
        const left = skierPosition.x - (Constants.GAME_WIDTH / 2);
        const top = skierPosition.y - (Constants.GAME_HEIGHT / 2);

        this.previousGameWindow = this.gameWindow;
        this.gameWindow = new Rect(left, top, left + Constants.GAME_WIDTH, top + Constants.GAME_HEIGHT);
        Engine.canvas.setDrawOffset(this.gameWindow.left, this.gameWindow.top);
    }

    destroyGameEntity(entity) {
        Engine.entities = Engine.entities.filter((val, idx, arr) => {
            return val != entity;
        });
    }
}

const game = new Game();

export default game;