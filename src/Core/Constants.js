export const GAME_WIDTH = window.innerWidth;
export const GAME_HEIGHT = window.innerHeight;

export const SKIER_CRASH = 'skierCrash';
export const SKIER_LEFT = 'skierLeft';
export const SKIER_LEFTDOWN = 'skierLeftDown';
export const SKIER_DOWN = 'skierDown';
export const SKIER_RIGHTDOWN = 'skierRightDown';
export const SKIER_RIGHT = 'skierRight';
export const SKIER_JUMP_1 = 'skierJump1';
export const SKIER_JUMP_2 = 'skierJump2';
export const SKIER_JUMP_3 = 'skierJump3';
export const SKIER_JUMP_4 = 'skierJump4';
export const SKIER_JUMP_5 = 'skierJump5';
export const RHINO_DEFAULT = 'rhinoDefault';
export const RHINO_RUN_L_1 = 'rhinoRun1';
export const RHINO_RUN_L_2 = 'rhinoRun2';
export const RHINO_RUN_R_1 = 'rhinoRunR1';
export const RHINO_RUN_R_2 = 'rhinoRunR2';
export const RHINO_EAT_1 = 'rhinoEat1';
export const RHINO_EAT_2 = 'rhinoEat2';
export const RHINO_EAT_3 = 'rhinoEat3';
export const RHINO_EAT_4 = 'rhinoEat4';
export const RHINO_EAT_5 = 'rhinoEat5';
export const RHINO_EAT_6 = 'rhinoEat6';
export const TREE = 'tree';
export const TREE_CLUSTER = 'treeCluster';
export const ROCK1 = 'rock1';
export const ROCK2 = 'rock2';
export const RAMP = 'ramp';

export const SKIER_STARTING_SPEED = 10;
export const ADDITIONAL_JUMP_SPEED = 2;
export const ENEMY_ENTRY_TIME = 5000;
export const ENEMY_STARTING_SPEED = 10.25;
export const ENEMY_SPEED_GAIN = .001;
// Diagonal movement needs normalization (√2)
export const SPEED_NORMALIZER = 1.4142;

export const ASSETS = {
    [SKIER_CRASH]: 'img/skier_crash.png',
    [SKIER_LEFT]: 'img/skier_left.png',
    [SKIER_LEFTDOWN]: 'img/skier_left_down.png',
    [SKIER_DOWN]: 'img/skier_down.png',
    [SKIER_RIGHTDOWN]: 'img/skier_right_down.png',
    [SKIER_RIGHT]: 'img/skier_right.png',
    [SKIER_JUMP_1]: 'img/skier_jump_1.png',
    [SKIER_JUMP_2]: 'img/skier_jump_2.png',
    [SKIER_JUMP_3]: 'img/skier_jump_3.png',
    [SKIER_JUMP_4]: 'img/skier_jump_4.png',
    [SKIER_JUMP_5]: 'img/skier_jump_5.png',
    [RHINO_DEFAULT]: 'img/rhino_default.png',
    [RHINO_RUN_L_1] : 'img/rhino_run_left.png',
    [RHINO_RUN_L_2] : 'img/rhino_run_left_2.png',
    [RHINO_RUN_R_1] : 'img/rhino_run_right.png',
    [RHINO_RUN_R_2] : 'img/rhino_run_right_2.png',
    [RHINO_EAT_1] : 'img/rhino_lift.png',
    [RHINO_EAT_2] : 'img/rhino_lift_mouth_open.png',
    [RHINO_EAT_3] : 'img/rhino_lift_eat_1.png',
    [RHINO_EAT_4] : 'img/rhino_lift_eat_2.png',
    [RHINO_EAT_5] : 'img/rhino_lift_eat_3.png',
    [RHINO_EAT_6] : 'img/rhino_lift_eat_4.png',
    [TREE] : 'img/tree_1.png',
    [TREE_CLUSTER] : 'img/tree_cluster.png',
    [ROCK1] : 'img/rock_1.png',
    [ROCK2] : 'img/rock_2.png',
    [RAMP] : 'img/jump_ramp.png'
};

export const ANIMATIONS = {
    SKIER_JUMP_ANIM : [
        SKIER_JUMP_1,
        SKIER_JUMP_2,
        SKIER_JUMP_3,
        SKIER_JUMP_4,
        SKIER_JUMP_5
    ],
    RHINO_RUN_L_ANIM : [
        RHINO_RUN_L_1,
        RHINO_RUN_L_2
    ],
    RHINO_RUN_R_ANIM : [
        RHINO_RUN_R_1,
        RHINO_RUN_R_2
    ],
    RHINO_CATCH_ANIM : [
        RHINO_EAT_1,
        RHINO_EAT_2,
        RHINO_EAT_3,
        RHINO_EAT_4,
        RHINO_EAT_5,
        RHINO_EAT_6
    ],
    RHINO_CELEBRATE_ANIM : [
        RHINO_EAT_5,
        RHINO_EAT_6
    ]
}

export const SKIER_DIRECTIONS = {
    CRASH : 0,
    LEFT : 1,
    LEFT_DOWN : 2,
    DOWN : 3,
    RIGHT_DOWN : 4,
    RIGHT : 5
};

export const SKIER_DIRECTION_ASSET = {
    [SKIER_DIRECTIONS.CRASH] : SKIER_CRASH,
    [SKIER_DIRECTIONS.LEFT] : SKIER_LEFT,
    [SKIER_DIRECTIONS.LEFT_DOWN] : SKIER_LEFTDOWN,
    [SKIER_DIRECTIONS.DOWN] : SKIER_DOWN,
    [SKIER_DIRECTIONS.RIGHT_DOWN] : SKIER_RIGHTDOWN,
    [SKIER_DIRECTIONS.RIGHT] : SKIER_RIGHT
};

export const KEYS = {
    LEFT : 37,
    RIGHT : 39,
    UP : 38,
    DOWN : 40,
    SPACE: 32
};