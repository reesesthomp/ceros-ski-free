import * as Constants from "./Constants";
import { AssetManager } from "./AssetManager";
import { Canvas } from './Canvas';
import { intersectTwoRects, Rect } from "../Core/Utils";

class Engine {
    // Array for tracking all game entities - Update, onCollision, Draw, and 
    // handleKeyDown called on all entities per frame
    entities = [];
    // Keys must be bound through Constants to assign proper controls
    boundKeys = [];
    // Track time and deltaTime
    time = 0;
    deltaTime = 0;

    constructor() {
        this.gameStartTime = Date.now();
        this.assetManager = new AssetManager();
        this.canvas = new Canvas(Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
        // Key assignment for preventDefault check
        for (const key in Constants.KEYS) {
            this.boundKeys.push(Constants.KEYS[key]);
        }
        document.addEventListener('keydown', this.handleKeyDown.bind(this));
    }

    async load() {
        await this.assetManager.loadAssets(Constants.ASSETS);
    }

    startScene(scene) {
        this.scene = scene;
        scene.start();
        this.run();
    }

    run() {
        // Update properties
        const now = Date.now() - this.gameStartTime;
        this.deltaTime = now - this.time;
        this.time = now;

        // Run game engine
        this.canvas.clearCanvas();
        this.updateGame();
        this.collisionsGame();
        this.drawGame();
        requestAnimationFrame(this.run.bind(this));
    }

    updateGame() {
        if (typeof this.scene.updateScene === 'function') {
            this.scene.updateScene();
        }
        this.entities.forEach((entity, idx) => {
            if (typeof entity.update === 'function') {
                entity.update();
            }
        });
    }

    collisionsGame() {
        this.entities.forEach((entity, idx) => {
            if (typeof entity.onCollision === 'function') {
                const asset = this.assetManager.getAsset(entity.assetName);
                const entityBounds = new Rect(
                    entity.x - asset.width / 2,
                    entity.y - asset.height / 2,
                    entity.x + asset.width / 2,
                    entity.y - asset.height / 4
                );

                const collision = this.entities.find((collider) => {
                    // Game Entities cannot collide with themselves
                    if (collider == entity) {
                        return;
                    }
                    const colliderAsset = this.assetManager.getAsset(collider.getAssetName());
                    const colliderPosition = collider.getPosition();
                    const colliderBounds = new Rect(
                        colliderPosition.x - colliderAsset.width / 2,
                        colliderPosition.y - colliderAsset.height / 2,
                        colliderPosition.x + colliderAsset.width / 2,
                        colliderPosition.y
                    );
                    
                    // Util checks for overlapping bounds - if true object has collision
                    if(intersectTwoRects(entityBounds, colliderBounds)) {
                        entity.onCollision(collider);
                    }
                });
            }
        });
        
    }

    drawGame() {
        if (typeof this.scene.drawScene === 'function') {
            this.scene.drawScene();
        }
        this.entities.forEach((entity, idx) => {
            if (typeof entity.draw === 'function') {
                entity.draw(this.canvas, this.assetManager);
            }
        });
    }

    handleKeyDown(event) {
        if (~this.boundKeys.indexOf(event.which)) {
            event.preventDefault();
        }
        if (typeof this.scene.onKeyDown === 'function') {
            this.scene.onSceneKeyDown();
        }
        this.entities.forEach((entity, idx) => {
            if (typeof entity.onKeyDown === 'function') {
                entity.onKeyDown(event.which);
            }
        });
    }
}

const engine = new Engine();

export default engine;